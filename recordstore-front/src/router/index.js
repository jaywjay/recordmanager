import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/Login.vue'
import Signup from '@/components/Signup.vue'

import Artists from '@/components/artists/Artists.vue'
import Records from '@/components/records/Records.vue'

import '@/assets/css/tailwind.css'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Login',
      component: Login
    },
    {
      path: '/signup',
      name: 'Signup',
      component: Signup
    },
    {
      path: '/artists',
      name: 'Artists',
      component: Artists
    },
    {
      path: '/records',
      name: 'Records',
      component: Records
    }
  ]
})
